<?php

namespace Hwp\Settings;

use Hwp\Settings\Hwp_Setting_Service;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * db-conception
 * 2021-02-06
 * 1.0
 * Hooks and functions for order admin page
 */
class Hwp_Setting_Order {

    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    public function __construct() {  
        $this->init();
    }

    public function init(){
        add_filter( 'manage_edit-shop_order_columns', [$this, 'set_custom_edit_shop_order_columns'] );
        add_action( 'manage_shop_order_posts_custom_column' , [$this, 'custom_shop_order_column'], 10, 2 );
        add_action( 'add_meta_boxes', [$this, 'add_shop_order_meta_box'] );
        add_action( 'save_post', [$this, 'save_shop_order_meta_box_data'] );
    }
  

    //from::https://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column

    // For displaying in columns.
    public function set_custom_edit_shop_order_columns($columns) {
        $columns['custom_column'] = __( 'Custom Column', 'your_text_domain' );
        return $columns;
    }

    // Add the data to the custom columns for the order post type:
    public function custom_shop_order_column( $column, $post_id ) {
        switch ( $column ) {

            case 'custom_column' :
                echo esc_html( get_post_meta( $post_id, 'custom_column', true ) );
                break;

        }
    }

    // For display and saving in order details page.
    public function add_shop_order_meta_box() {

        add_meta_box(
            'custom_column',
            __( 'Custom Column', 'your_text_domain' ),
            [$this, 'shop_order_display_callback'],
            'shop_order',
            "side"
        );

    }

    // For displaying.
    public function shop_order_display_callback( $post ) {

        $value = get_post_meta( $post->ID, 'custom_column', true );

        echo '<textarea style="width:100%" id="custom_column" name="custom_column">' . esc_attr( $value ) . '</textarea>';
    }

    // For saving.
    public function save_shop_order_meta_box_data( $post_id ) {

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        // Check the user's permissions.
        if ( isset( $_POST['post_type'] ) && 'shop_order' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_shop_order', $post_id ) ) {
                return;
            }
        }

        // Make sure that it is set.
        if ( ! isset( $_POST['custom_column'] ) ) {
            return;
        }

        // Sanitize user input.
        $my_data = sanitize_text_field( $_POST['custom_column'] );

        // Update the meta field in the database.
        update_post_meta( $post_id, 'custom_column', $my_data );
    }

   
}