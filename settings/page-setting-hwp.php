<?php

use hwp\Settings\hwp_Setting_Service;

$settings_serv = hwp_Setting_Service::getInstance();
$debugMode = $settings_serv->getDebug();
?>

<h1>
    <?php esc_html_e( "Paramètre d'Holded WooCommerce Plus", 'my-plugin-textdomain' ); ?>
</h1>

<form method="POST" action="<?= admin_url( 'admin-post.php' ) ?>">
    <?= wp_nonce_field("main_settings_form_submit") ?>
    <input type="hidden" name="action" value="main_settings_form_submit">
    <input type="checkbox" name="debug_mode" <?= $debugMode ? "checked" : ""?> >
    <label for="debug_mode">Mode debug</label>
    <br>
    <input type="submit" value="Enregistrer">
</form>

