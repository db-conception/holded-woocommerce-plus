<?php

namespace Hwp\Settings;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Dbc_Settings_Service{
    

    protected function getValue($key, $group =''){
        $found = false;
        $cached = wp_cache_get($key, $group, false, $found);
        if($found === false){
            // get from database
            $fromDb = get_option($key);

            // save in cache for next access 
            wp_cache_set($key, $fromDb, $group);

            return $fromDb;
        }
        return $cached;
    }

    protected function setValue($key, $data, $group =''){
        update_option($key, $data);
        wp_cache_set($key, $data, $group);
    }

}