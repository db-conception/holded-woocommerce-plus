<?php

namespace Hwp\Settings;

use Hwp\Settings\Hwp_Setting_Service;
use Hwp\Settings\Dbc_Setting_Admin;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * db-conception
 * 2021-02-04
 * 1.0
 * Provide functions to set/get customs settings of hte plugin or class
 */
class Hwp_Setting_Admin extends Dbc_Setting_Admin{

    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    public function __construct() {  
        $this->set_admin_menu_title("Holded Wc Plus");
        $this->set_admin_page_slug("settings-hwp");
        $this->set_admin_page_title("Paramètre d'Holded WooCommerce Plus'");
        // $this->set_admin_menu_icon();
        // $this->set_admin_menu_priority();
        $this->set_text_domaine("hwp");

        $this->init();
    }

    public function init(){
        parent::init();

        add_action( 'admin_post_main_settings_form_submit', [$this, 'main_settings_form_submit']);
    }
    public function admin_page_contents() {
        require_once("page-setting-hwp.php");
    }

    public function main_settings_form_submit(){
        if( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'main_settings_form_submit') ) {
    
            // sanitize the input
            if( isset($_POST["debug_mode"])){
                $modeDebug = $_POST["debug_mode"];
                $modeDebug = \in_array($modeDebug, [1, "yes", "true", true]);
                \hwp_log("setDebug to ");
                \hwp_log($modeDebug);
            }

            // do the processing
            $service = Hwp_Setting_Service::getInstance();
            $service->setDebug( $modeDebug );

            // add the admin notice
            $admin_notice = "success";
    
            // redirect the user to the appropriate page
            wp_redirect( $_SERVER["HTTP_REFERER"], 302, 'WordPress' );
            exit;
            // $this->custom_redirect( $admin_notice, $_POST );
            // exit;
        }			
        else {
            wp_die( __( 'Invalid nonce specified', $this->get_admin_page_slug() ), 
                    __( 'Error', $this->get_admin_page_slug() ), array(
                        'response' 	=> 403,
                        'back_link' => 'admin.php?page=' . $this->get_admin_page_slug(),
    
                ) );
        }
    }

    public function register_admin_scripts() {
        //wp_register_style( 'my-plugin', plugins_url( 'ddd/css/plugin.css' ) );
        //wp_register_script( 'admin-owlana', get_template_directory_uri()."-child/admin-owlana.js", array( 'jquery', 'wp-color-picker' ), false, true);
    }
    
    
    public function load_scripts( $hook ) {
        // Load only on ?page=sample-page
        // var_dump( $hook );
        if( $hook != 'toplevel_page_'.$this->get_admin_page_slug() ) {
            return;
        }
    
        // Load style & scripts.
        // wp_enqueue_style( 'wp-color-picker' );
        // wp_enqueue_style( 'my-plugin' );
        // wp_enqueue_script( 'admin-owlana' );
    }
   
}