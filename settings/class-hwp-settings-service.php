<?php

namespace Hwp\Settings;

use Hwp\Settings\Dbc_Settings_Service;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * db-conception
 * 2021-02-04
 * 1.0
 * Provide functions to set/get customs settings of Owlana.fr
 */
class Hwp_Setting_Service extends Dbc_Settings_Service{

    private static $_instance = null;

    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    private function __construct() {  
    }

    /*
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return Singleton
    */
    public static function getInstance() {
 
        if(is_null(self::$_instance)) {
            self::$_instance = new Hwp_Setting_Service();  
        }

        return self::$_instance;
    }

    //const DEBUG_KEY = \Holded_WooCommerce_Plus::DEBUG_KEY;

    /**
     * Get the color in string hexa format
     * or false
     */
    public function getDebug(){
        $debug = $this->getValue(\Holded_WooCommerce_Plus::DEBUG_KEY);
        if(in_array($debug, [null, false, '', 0]))
            return false;
        return $debug;
    }

    /**
     * Set the html content of the slide 1
     */
    public function setDebug($enabled){
        $this->setValue(\Holded_WooCommerce_Plus::DEBUG_KEY, $enabled);
    }

}