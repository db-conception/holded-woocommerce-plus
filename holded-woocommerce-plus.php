<?php
/*
   Plugin Name: Holded WooCommerce Plus
   Plugin URI: https://gitlab.com/db-conception/holded-woocommerce-plus
   Version: 1
   Author: db-conception
   Description: Plugin WooCommerce for additionals features.
   Text Domain: hwp
   License: GPLv3
*/

use Hwp\Settings\hwp_Setting_Admin;
use Hwp\Settings\Hwp_Setting_Order;

defined( 'ABSPATH' ) or die( "dude");

if( ! class_exists("Holded_WooCommerce_Plus")){
    class Holded_WooCommerce_Plus {
        
        const DEBUG_KEY = "hwp_debug";
        const ORDER_INVOICE_NUM_KEY = "ORDER_INVOICE_NUM";
        const ORDER_INVOICE_ID_KEY = "ORDER_INVOICE_ID";
        const ORDER_INVOICE_LINK_KEY = "ORDER_INVOICE_LINK";

        public $hwp_debug = true;
        
        public $settings = null;
        public $order_admin = null;
        public $controller = null;
        public $notice = null;
        
        function __construct() {
            $this->includes();

            $this->notice = new Dbc_Notice();
            $this->settings = new Hwp_Setting_Admin();
            $this->order_admin = new Hwp_Setting_Order();
            $this->controller = new HWP_Rest_Invoice_Controller();
            $this->hwp_debug = get_option(self::DEBUG_KEY) === "yes" ? true : false;

            $this->hooks();
        }
        function activate(){
        }
        function deactivate(){
            $timestamp = wp_next_scheduled( 'my_schedule_hook' );
            wp_unschedule_event($timestamp, "hwp_sync_stock_cron");
        }
        function uninstall(){

        }

        function includes(){
            //class 
            if(class_exists("Dbc_Notice") === false){
                include_once( "includes/dbc_notice.php");
            }
            
            require_once("settings/class-dbc-settings-service.php");
            require_once("settings/class-hwp-settings-service.php");
            require_once("settings/class-dbc-settings-admin.php");
            require_once("settings/class-hwp-settings-admin.php");
            require_once("settings/class-hwp-settings-order.php");
            require_once("includes/HWP_Rest_Invoice_Controller.php");
        }   

        function hooks(){
            add_action("hwp_sync_stock_cron", [$this, "sync_stock"]);
            add_filter("holded_update_invoice_fields", [$this, "update_invoice_fields_filter"], 10, 1);
            add_action("holded_salesorder_created", [$this, "link_invoice_to_order"], 10, 2);
            $this->setup_cron();
        }

        public function setup_cron(){
            add_filter( 'cron_schedules', [$this, 'add_cron_interval'] );
            if(wp_next_scheduled("hwp_sync_stock_cron") == false) {
                hwp_log("next schedule false");
                $ts = time();
                if(!wp_schedule_event( $ts, "everyminute", 'hwp_sync_stock_cron' )){
                    $mess = "error during the scheduling of holded woocommerce plus";
                    hwp_log($mess);
                    wp_mail(get_option("admin_email"), "ERROR SYNC STOCK", $mess);
                }
            }
        }

        public function add_cron_interval( $schedules ){
            $schedules['everyminute'] = array(
                    'interval'  => 60, // time in seconds
                    'display'   => 'Every Minute'
            );
            return $schedules;
        }

        public function sync_stock(){
            hwp_log("cron executed");

            $holded_response = wp_remote_get('https://api.holded.com/api/invoicing/v1/products',
                [
                    "headers" => ["key" => "fe46195c16faaad360b95aecef794ac0"]
                ]
            );
            if($holded_response["response"]["code"] === 200){
                $json =  wp_remote_retrieve_body( $holded_response );
                hwp_log("responsebody " ,$json);
                $products = json_decode( $json );
                foreach ($products as $holded_product) {
                    if( is_numeric( $holded_product->sku ) ){
                        hwp_log("sku : ".$holded_product->sku);
                        $id = wc_get_product_id_by_sku( $holded_product->sku );
                        hwp_log("id : $id");

                        $product = wc_get_product($id);                
                        if($product !== null){
                            wc_update_product_stock( $product, $holded_product->stock );
                            hwp_log($product->get_title()." as been restocked of ".$holded_product->stock );
                        }
                        else{
                            hwp_log("product not found id : $id for sku ".$holded_product->sku);
                        }
                    }
                }
            }
        }

        public function update_invoice_fields_filter( $fields ){
            $toFilter = ["wc-processing"];
            hwp_log("th_holded_update_invoice_fields_filter");
            hwp_log($fields["woocommerceOrderStatus"]);
            
            if(empty($fields["woocommerceOrderStatus"]) || 
                !in_array($fields["woocommerceOrderStatus"], $toFilter))
                return $fields;
            
            $fields["woocommerceOrderStatus"] = "wc-completed";

            return $fields;
        }
        
        public function link_invoice_to_order($results, $order_id){
            /* results
            Array
            (
                [status] => 1
                [id] => 6065e3ad99cc3e31ba6d6401
                [invoicenum] => F210007
                [contactid] => 605a23a13d17d7741e01cefb
            )
            */
            if(isset( $results["status"]) && 
            $results["status"] == 1 && 
            isset($results["id"]) && 
            isset($results["invoicenum"])){

                $order = wc_get_order( $order_id );
                $success = update_post_meta($order_id, self::ORDER_INVOICE_NUM_KEY, $results["invoicenum"]);
                $success = $success && update_post_meta($order_id, self::ORDER_INVOICE_ID_KEY, $results["id"]);
                $success = $success && update_post_meta($order_id, self::ORDER_INVOICE_LINK_KEY, $this->buildInvoiceLink($results["id"]) );
            }
        }

        public function buildInvoiceLink($invoice_id){
            // TODO get link
            return "https://taahir.fr/?download=".$invoice_id;
        }

        public static function download_invoice($invoice_id) {
            hwp_log("download_invoice");
            if (isset($invoice_id)) {

                $holded_response = wp_remote_get("https://api.holded.com/api/invoicing/v1/documents/invoice/$invoice_id/pdf",
                    [
                        "headers" => ["key" => "fe46195c16faaad360b95aecef794ac0"]
                    ]
                );
                $json =  wp_remote_retrieve_body( $holded_response );
                $holded_response = json_decode( $json ); 
                if(isset($holded_response->status) && 
                $holded_response->status == 1 && 
                isset($holded_response->data) ){

                    return  base64_decode($holded_response->data);
                }
                hwp_log( $holded_response );
                throw new Exception("Impossible de retrouver la facture");
            }
        }
    }
}
if( ! function_exists("hwp_log")){
    function hwp_log($title, $message = null)
    {
        $hwp_debug = get_option(Holded_WooCommerce_Plus::DEBUG_KEY) === "yes" ? true : false;
        if(false === $hwp_debug)
            return;
        error_log(print_r($title, true));
        if(null !== $message)
          error_log(print_r($message, true));
    }
}


$hwp = new Holded_WooCommerce_Plus();

register_activation_hook(__FILE__ , array($hwp, "activate"));

register_deactivation_hook(__FILE__, array($hwp, "deactivate"));
