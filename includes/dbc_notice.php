<?php

defined( 'ABSPATH' ) or die( "dude");

if( !class_exists("Dbc_Notice")){
    /**
     * Sample_Notice_Handling
     * https://wordpress.stackexchange.com/questions/240733/displaying-one-time-notification-in-plugins 
     */
    class Dbc_Notice {

        public static $_notices  = array();

        /**
         * Constructor
         */
        public function __construct() {

            add_action( 'admin_notices', array( $this, 'output_errors' ) );
            add_action( 'shutdown', array( $this, 'save_errors' ) );
        }

        /**
         * Add an error message
         */
        public static function add_error( $text ) {
            self::$_notices[] = $text;
        }

        /**
         * Save errors to an option
         */
        public function save_errors() {
            update_option( 'custom_notices', self::$_notices );
        }

        /**
         * Show any stored error messages
         */
        public function output_errors() {
            $errors = maybe_unserialize( get_option( 'custom_notices' ) );

            if ( ! empty( $errors ) ) {

                echo '<div id="mc_errors" class="error notice is-dismissible">';

                foreach ( $errors as $error ) {
                    echo '<p>' . wp_kses_post( $error ) . '</p>';
                }

                echo '</div>';

                // Clear
                delete_option( 'custom_notices' );
            }
        }

    }

}