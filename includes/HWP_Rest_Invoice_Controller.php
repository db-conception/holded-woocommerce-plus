<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * db-conception
 * 06/04/2021
 */


class HWP_Rest_Invoice_Controller extends WP_REST_Controller {

    const NAMESPACE = 'hwp/v1';
    const DL_INVOICE_PATH = 'invoice/(?P<invoice_id>[a-zA-Z0-9-]+)';

    
    public function __construct() {
        add_action("rest_api_init", [$this, "register_routes"]);
    }
    
    public function register_routes() {
        register_rest_route( self::NAMESPACE, '/' . self::DL_INVOICE_PATH, [
        array(
          'methods'             => 'GET',
          'callback'            => array( $this, 'download_invoice' ),
          'permission_callback' => array( $this, 'download_invoice_check' )
              ),
        ]);

    }

    public function download_invoice( $request ){

        $invoice_id = $request->get_param( "invoice_id" );
        $filedata = Holded_WooCommerce_Plus::download_invoice($invoice_id);
        header("Content-type:application/pdf");
        header("X-INVOICEPATH:" . HWP_Rest_Invoice_Controller::getDownloadInvoicePath( $invoice_id ) );
        header("Content-Disposition:attachment;filename=facture_taahir_$invoice_id.pdf");
        // header('Content-Length: ' . sizeof($filedata));
        echo $filedata;
        exit;

    }

    public function download_invoice_check( $request ){
        return true;
    }

    public static function getDownloadInvoicePath( $invoice_id ){
        $end = str_replace("(?P<invoice_id>[a-zA-Z0-9-]+)", $invoice_id, self::DL_INVOICE_PATH);
        return get_site_url(null, "wp-json/".self::NAMESPACE."/$end");
    }
    
  }